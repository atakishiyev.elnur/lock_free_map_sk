/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   data_obj.h
 * Author: root
 *
 * Created on July 21, 2016, 10:57 AM
 */

#ifndef DATA_OBJ_H
#define DATA_OBJ_H

#include "Scheduler.h"
#include <jni.h>
#include <string>
#include <stdio.h>

using namespace std;

class data_obj {
public:

    data_obj(int length, string key, char* _data, int version) : length(length), data(_data), key(key),  version(version) {
    }

    data_obj(data_obj &orig) {

    }

    ~data_obj() {
        this->length = 0;
        free(this->data);
        this->data = NULL;
    }



    long task = -1;
    int length = 0;
    int version = 0;
    char* data = nullptr;
    string key = "";
};


#endif /* DATA_OBJ_H */

