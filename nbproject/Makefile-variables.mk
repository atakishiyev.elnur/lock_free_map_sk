#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=GNU-Linux
CND_ARTIFACT_DIR_Debug=dist/Debug/GNU-Linux
CND_ARTIFACT_NAME_Debug=liblock_free_map_sk.so
CND_ARTIFACT_PATH_Debug=dist/Debug/GNU-Linux/liblock_free_map_sk.so
CND_PACKAGE_DIR_Debug=dist/Debug/GNU-Linux/package
CND_PACKAGE_NAME_Debug=liblockfreemapsk.so.tar
CND_PACKAGE_PATH_Debug=dist/Debug/GNU-Linux/package/liblockfreemapsk.so.tar
# Release configuration
CND_PLATFORM_Release=GNU-Linux
CND_ARTIFACT_DIR_Release=dist/Release/GNU-Linux
CND_ARTIFACT_NAME_Release=liblock_free_map_sk.so
CND_ARTIFACT_PATH_Release=dist/Release/GNU-Linux/liblock_free_map_sk.so
CND_PACKAGE_DIR_Release=dist/Release/GNU-Linux/package
CND_PACKAGE_NAME_Release=liblockfreemapsk.so.tar
CND_PACKAGE_PATH_Release=dist/Release/GNU-Linux/package/liblockfreemapsk.so.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
