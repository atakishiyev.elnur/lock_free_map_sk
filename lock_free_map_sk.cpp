/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#define _DEFAULT_CAPACITY  1000
#include <mutex>

#include <atomic>
#include <vector>
#include "linkedlogics_offheap_StringKeyConcurrentHashMap.h"
#include <jni.h>
#include <stdio.h>
#include <iostream>
#include <unordered_map>
#include <string.h>
#include <chrono>
#include <cmath>
#include <thread>
#include <jemalloc/jemalloc.h>
#include "data_obj.h"
#include "map_metadata.h"
#include <mutex>

using namespace std;

unordered_map<int, map_metadata*> maps;
//map_metadata** maps = new map_metadata*[1024];
std::atomic_int idx{0};
mutex _mutex;

void GetJStringContent(JNIEnv *env, jstring AStr, std::string &ARes) {
    if (!AStr) {
        ARes.clear();
        return;
    }

    const char *s = env->GetStringUTFChars(AStr, 0);
    ARes = s;
    env->ReleaseStringUTFChars(AStr, s);
}

JNIEXPORT jint JNICALL Java_linkedlogics_offheap_StringKeyConcurrentHashMap_createMap__
(JNIEnv *env, jobject) {
    std::unique_lock<mutex> lock(_mutex);
    //    maps[idx] = new map_metadata();
    maps.insert(std::pair<int, map_metadata*>(idx.load(), new map_metadata()));
    return idx.fetch_add(1);
}

JNIEXPORT jint JNICALL Java_linkedlogics_offheap_StringKeyConcurrentHashMap_createMap__I
(JNIEnv *env, jobject, jint size) {
    std::unique_lock<mutex> lock(_mutex);
    //    maps[idx] = new map_metadata(size);
    maps.insert(std::pair<int, map_metadata*>(idx.load(), new map_metadata(size)));
    return idx.fetch_add(1);
}

JNIEXPORT jboolean JNICALL Java_linkedlogics_offheap_StringKeyConcurrentHashMap_put__ILjava_lang_String_2_3BJI
(JNIEnv *env, jobject obj, jint id, jstring key, jbyteArray value, jlong expire, jint version) {
    try {
        std::unique_lock<mutex> lock(_mutex);
        map_metadata* _map_metadata = maps[id];
        if (_map_metadata) {

            int len = env->GetArrayLength(value);
            std::string sk;
            GetJStringContent(env, key, sk);


            std::unordered_map<string, data_obj*> * map = _map_metadata->map;

            std::unordered_map<string, data_obj*>::const_iterator iter = map->find(sk);
            if (iter != map->end()) {
                if (iter->second->version < version || version < 0) {

                    jbyte* a = (jbyte*) env->GetPrimitiveArrayCritical(value, NULL);
                    char* b = (char*) malloc(len);
                    memmove(b, a, len);

                    data_obj *dobj = iter->second;
                    _map_metadata->scheduler->cancel(iter->second->task);

                    _map_metadata->size_in_bytes -= dobj->length;
                    int _version = version;

                    if (_version < 0) {
                        _version = dobj->version;
                    }

                    delete dobj;

                    map->erase(iter);

                    data_obj *_data_obj = new data_obj(len, sk, b, _version);
                    map->operator[](sk) = _data_obj;
                    _map_metadata->size_in_bytes += len;

                    _data_obj->task = _map_metadata->scheduler->schedule(seconds(expire), &map_metadata::onExpired, _map_metadata, sk);
                    env->ReleasePrimitiveArrayCritical(value, a, JNI_ABORT);
                    return true;
                }
                return false;
            }
            jbyte* a = (jbyte*) env->GetPrimitiveArrayCritical(value, NULL);
            char* b = (char*) malloc(len);
            memmove(b, a, len);


            data_obj *_data_obj = new data_obj(len, sk, b, version);
            map->operator[](sk) = _data_obj;
            _map_metadata->size_in_bytes += len;

            _data_obj->task = _map_metadata->scheduler->schedule(seconds(expire), &map_metadata::onExpired, _map_metadata, sk);

            env->ReleasePrimitiveArrayCritical(value, a, JNI_ABORT);

            return true;
        } else return false;
    } catch (exception& e) {
        return false;
    }
}

JNIEXPORT jboolean JNICALL Java_linkedlogics_offheap_StringKeyConcurrentHashMap_put__ILjava_lang_String_2_3BI
(JNIEnv * env, jobject, jint id, jstring key, jbyteArray value, jint version) {
    try {
        std::unique_lock<mutex> lock(_mutex);
        map_metadata* _map_metadata = maps[id];
        if (_map_metadata) {

            int len = env->GetArrayLength(value);
            std::string sk;
            GetJStringContent(env, key, sk);

            std::unordered_map<string, data_obj*> * map = _map_metadata->map;

            std::unordered_map<string, data_obj*>::iterator iter = map->find(sk);
            if (iter != map->end()) {
                if (iter->second->version < version || version < 0) {
                    jbyte* a = (jbyte*) env->GetPrimitiveArrayCritical(value, NULL);
                    char* b = (char*) malloc(len);
                    memmove(b, a, len);

                    data_obj* dobj = iter->second;
                    _map_metadata->scheduler->cancel(iter->second->task);

                    _map_metadata->size_in_bytes -= dobj->length;

                    int _version = version;

                    if (_version < 0) {
                        _version = dobj->version;
                    }

                    delete dobj;
                    map->erase(iter);

                    map->operator[](sk) = new data_obj(len, sk, b, _version);
                    _map_metadata->size_in_bytes += len;

                    env->ReleasePrimitiveArrayCritical(value, a, JNI_ABORT);
                    return true;
                }
                return false;
            }

            jbyte* a = (jbyte*) env->GetPrimitiveArrayCritical(value, NULL);
            char* b = (char*) malloc(len);
            memmove(b, a, len);

            map->operator[](sk) = new data_obj(len, sk, b, version);
            _map_metadata->size_in_bytes += len;

            env->ReleasePrimitiveArrayCritical(value, a, JNI_ABORT);

            return true;
        } else return false;
    } catch (exception) {
        return false;
    }
}

JNIEXPORT jbyteArray JNICALL Java_linkedlogics_offheap_StringKeyConcurrentHashMap_get
(JNIEnv * env, jobject, jint id, jstring key) {
    std::unique_lock<mutex> lock(_mutex);
    jbyteArray bArray = NULL;
    map_metadata* _map_metadata = maps[id];
    if (_map_metadata) {
        std::string sk;
        GetJStringContent(env, key, sk);

        std::unordered_map<string, data_obj*>* map = _map_metadata->map;
        std::unordered_map<string, data_obj*>::const_iterator iter = map->find(sk);

        if (iter == map->end()) {
            
        } else {
            data_obj* _data_ = iter->second;
            if (_data_->length > 0) {
                bArray = env->NewByteArray(_data_->length);
                if (bArray) {
                    env->SetByteArrayRegion(bArray, 0, _data_->length, (jbyte*) _data_->data);
                }
            }
        }
    }
    return bArray;
}

JNIEXPORT void JNICALL Java_linkedlogics_offheap_StringKeyConcurrentHashMap_remove__ILjava_lang_String_2
(JNIEnv * env, jobject, jint id, jstring key) {
    std::unique_lock<mutex> lock(_mutex);
    map_metadata* _map_metadata = maps[id];
    if (_map_metadata) {
        std::string sk;
        GetJStringContent(env, key, sk);

        std::unordered_map<string, data_obj*> * map = _map_metadata->map;
        std::unordered_map<string, data_obj*>::iterator iter = map->find(sk);

        if (iter != map->end()) {
            data_obj* dobj = iter->second;
            _map_metadata->scheduler->cancel(iter->second->task);
            map->erase(iter);
            _map_metadata->size_in_bytes -= dobj->length;
            delete dobj;
        }
    }
}

JNIEXPORT void JNICALL Java_linkedlogics_offheap_StringKeyConcurrentHashMap_remove__ILjava_lang_String_2I
(JNIEnv * env, jobject, jint id, jstring key, jint version) {
    std::unique_lock<mutex> lock(_mutex);
    map_metadata *_map_metadata = maps[id];
    if (_map_metadata) {
        std::string sk;
        GetJStringContent(env, key, sk);

        std::unordered_map<string, data_obj*> * map = _map_metadata->map;
        std::unordered_map<string, data_obj*>::iterator iter = map->find(sk);

        if (iter != map->end() && iter->second->version <= version) {
            data_obj* dobj = iter->second;
            _map_metadata->scheduler->cancel(iter->second->task);
            map->erase(iter);
            _map_metadata->size_in_bytes -= dobj->length;
            delete dobj;
        }
    }
}

JNIEXPORT jlong JNICALL Java_linkedlogics_offheap_StringKeyConcurrentHashMap_size
(JNIEnv *, jobject, jint id) {
    std::unique_lock<mutex> lock(_mutex);
    map_metadata* _map_metadata = maps[id];
    if (_map_metadata) {
        std::unordered_map<string, data_obj*>* map = _map_metadata->map;
        return map->size();
    } else {
        return -1;
    }
}

JNIEXPORT jlong JNICALL Java_linkedlogics_offheap_StringKeyConcurrentHashMap_sizeInBytes
(JNIEnv *, jobject, jint id) {
    std::unique_lock<mutex> lock(_mutex);
    map_metadata* _map_metadata = maps[id];
    if (_map_metadata) {
        return _map_metadata->size_in_bytes;
    } else {
        return -1;
    }
}

JNIEXPORT void JNICALL Java_linkedlogics_offheap_StringKeyConcurrentHashMap_setEvictionHandler
(JNIEnv * env, jobject, jint id, jobject handler) {
}

JNIEXPORT void JNICALL Java_linkedlogics_offheap_StringKeyConcurrentHashMap_suspendEviction
(JNIEnv *, jobject, jint id) {
    std::unique_lock<mutex> lock(_mutex);
    map_metadata* _map_metadata = maps[id];
    if (_map_metadata) {
        _map_metadata->scheduler->suspend();
    }

}

JNIEXPORT void JNICALL Java_linkedlogics_offheap_StringKeyConcurrentHashMap_resumeEviction
(JNIEnv *, jobject, jint id) {
    std::unique_lock<mutex> lock(_mutex);
    map_metadata* _map_metadata = maps[id];
    if (_map_metadata) {
        _map_metadata->scheduler->resume();
    }
}

JNIEXPORT jstring JNICALL Java_linkedlogics_offheap_StringKeyConcurrentHashMap_consumeEvictions
(JNIEnv *env, jobject, jint id) {
    std::unique_lock<mutex> lock(_mutex);
    map_metadata* _map_metadata = maps[id];
    if (_map_metadata) {
        return env->NewStringUTF(_map_metadata->consumeEvictionsBlocking().c_str());
    }
    return NULL;
}

JNIEXPORT jstring JNICALL Java_linkedlogics_offheap_StringKeyConcurrentHashMap_consumeEvictionsNonBlocking
(JNIEnv * env, jobject, jint id) {
    std::unique_lock<mutex> lock(_mutex);
    map_metadata* _map_metadata = maps[id];
    if (_map_metadata) {
        return env->NewStringUTF(_map_metadata->consumeEvictionsNonBlocking().c_str());
    }
    return NULL;
}

JNIEXPORT void JNICALL Java_linkedlogics_offheap_StringKeyConcurrentHashMap_reset
(JNIEnv *, jobject, jint id, jint size) {
    std::unique_lock<mutex> lock(_mutex);
    map_metadata* _map_metadata = maps[id];
    if (_map_metadata) {
        if (size > 0) {
            _map_metadata->reset(size);
        }else{
            delete _map_metadata;
        }
    }
}
