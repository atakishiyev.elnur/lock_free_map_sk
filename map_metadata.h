/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   map_metadata.h
 * Author: root
 *
 * Created on July 21, 2016, 10:56 AM
 */

#ifndef MAP_METADATA_H
#define MAP_METADATA_H

#include <map>
#include <jni.h>
#include <string.h>

#include "data_obj.h"
#include "Scheduler.h"
#include <queue>
#include <unordered_map>

using namespace std;

class map_metadata {
public:
    ulong size_in_bytes = 0;

    map_metadata() : map(new unordered_map<string, data_obj*>()), 
    scheduler(new Scheduler(1)), eviction_queue(), eviction_queue_con_var(), 
    eviction_queue_mutex() {

    }

    map_metadata(size_t size) : map(new unordered_map<string, data_obj*>(size)), 
    scheduler(new Scheduler(1)), eviction_queue(), eviction_queue_con_var(), 
    eviction_queue_mutex() {

    }

    ~map_metadata() {
        map->clear();
        delete scheduler;
        delete map;
    }

    map_metadata(map_metadata &orig) {
        cout << "Copy constructor called" << endl;
    }

    string consumeEvictionsBlocking() {
        unique_lock<mutex> _lock(eviction_queue_mutex);
        eviction_queue_con_var.wait(_lock, [this] {
            return !eviction_queue.empty();
        });
        string key = eviction_queue.front();
        eviction_queue.pop();
        return key;

    }

    void reset(int size) {
        map->clear();
        delete map;
        map = new unordered_map<string, data_obj*>(size);
    }

    string consumeEvictionsNonBlocking() {
        unique_lock<mutex> _lock(eviction_queue_mutex);
        if (eviction_queue.empty()) {
            return nullptr;
        }
        string key = eviction_queue.front();
        eviction_queue.pop();
        return key;

    }

    void onExpired(string key) {
        std::unique_lock<mutex> _lock(eviction_queue_mutex);
        eviction_queue.push(key);
        eviction_queue_con_var.notify_one();

    }


    mutex eviction_queue_mutex;
    condition_variable eviction_queue_con_var;
    queue<string> eviction_queue;

    unordered_map<string, data_obj*>* map;
    Scheduler *scheduler;
private:
};


#endif /* MAP_METADATA_H */

